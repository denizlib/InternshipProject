package notes

import (
	"fmt"
	"net/http"
	"log"
	"encoding/json"
	"orangenote/tools"
	"github.com/jinzhu/gorm"
	_ "github.com/lib/pq"
	//_ "github.com/jinzhu/gorm/dialects/postgres"
	"path"
	"strconv"
	jwt "github.com/dgrijalva/jwt-go"
	"os"
	"errors"
	"io/ioutil"
	"github.com/dgrijalva/jwt-go/request"
	"crypto/rsa"
	"time"
	"math/rand"
	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
	"github.com/lib/pq"
)




//	ERRORS
/*
	const ErrorCode0 string = "Success"
	const ErrorCode1 string = "Note Not Found"
	const ErrorCode2 string = "Invalid Json Note Type"
	const ErrorCode3 string = "Database Error"
	const ErrorCode4 string = ""
	const ErrorCode5 string = "Token Failure"
	const ErrorCode6 string = ""
	const ErrorCode7 string = ""
	const ErrorCode8 string = ""
*/
//	ERRORS



const (
	privKeyPath = "orangenotes/demo"     // openssl genrsa -out app.rsa keysize
	pubKeyPath  = "orangenotes/demo.pub"
)

var (
	privateKey *rsa.PrivateKey
	publicKey  *rsa.PublicKey
	//db	*gorm.DB
	err	error
	MissingJSONErr = errors.New("invalid json key")
	InvalidErr = errors.New("invalid id")
)

type ContextStruct struct{
	JwtToken	string 		`json:"jwtToken"`
}

type LoginErrorHandler struct{
	ErrorMessage string	 	`json:"message"`
	ErrorCode int 			`json:"code"`
	Context ContextStruct	`json:"context,omitempty"`
}

type ErrorHandlerArray struct{
	ErrorMessage string 	`json:"message"`
	ErrorCode	int			`json:"code"`
	ValueArray	[]Note        `json:"context,omitempty"`
}

type ErrorHandler struct{
	ErrorMessage string 	`json:"message"`
	ErrorCode	int			`json:"code"`
	Values 		*Note        `json:"context,omitempty"`
}

type Note struct {
	ID        uint64 				`json:"noteID" gorm:"primary_key"`
	CreatedAt time.Time				`json:"createdAt"`
	UpdatedAt time.Time				`json:"updatedAt,omitemtpy"`
	DeletedAt *time.Time			`json:"-"`
	//ID   uint64 					`json:"noteID"`
	Note string 					`json:"note"`
	UserID uint64					`json:"userID,omitempty"`
	IsPublic string					`json:"isPublic,omitempty"`
	SharedUsers pq.StringArray		`json:"sharedUsers,omitempty" gorm:"type:varchar(100)"` 	//json array
	Tags	pq.StringArray			`json:"tags,omitempty" gorm:"type:varchar(100)"`			//json array
}

type User struct {
	UserID uint64					`json:"userID,omitempty" gorm:"primary_key"`
	//gorm.Model
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time
	Username string					`json:"username,omitempty"`
	Password string 				`json:"password,omitempty"`
}


type changePwRequest struct {
	gorm.Model
	UserID			uint64
	OTP				int
}

type changePwConfirm struct{
	RequestingUsername	string 		`json:"username"`
	NewPassword			string		`json:"password"`
	OTP					int			`json:"otp"`
}

func init() {
	privateByte, _ := ioutil.ReadFile("demo.rsa")
	publicByte, _ := ioutil.ReadFile("demo.rsa.pub")
	privateKey, _ = jwt.ParseRSAPrivateKeyFromPEM(privateByte)
	publicKey, _ = jwt.ParseRSAPublicKeyFromPEM(publicByte)
	tools.DB.CreateTable(&Note{})
	tools.DB.CreateTable(&User{})
	tools.DB.CreateTable(&tools.LoggerToDB{})
	tools.DB.CreateTable(&changePwRequest{})
}

func (Note) TableName() string{
	return "notes"
}

func (changePwRequest) TableName() string{
	return "changepwrequests"
}

func AuthMiddleware(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	w.Header().Set("Content-Type", "application/json")
	var checkError ErrorHandler
	token, err := request.ParseFromRequest(r, request.OAuth2Extractor, func(token *jwt.Token) (interface{}, error) {
		return publicKey, nil
	})
	if err == nil && token.Valid {
		next(w, r)
	} else {
		var loggerToDBObj tools.LoggerToDB
		url := fmt.Sprintf("%v", r.URL)
		loggerToDBObj.Method=r.Method
		loggerToDBObj.Path=url
		loggerToDBObj.IP=r.RemoteAddr
		loggerToDBObj.Status=strconv.Itoa(http.StatusUnauthorized)
		logObject := tools.FormatRequest(r)
		w.WriteHeader(http.StatusUnauthorized)
		checkError.ErrorMessage="Token Failure"
		checkError.ErrorCode = 5
		jsonResp, _ := json.Marshal(checkError)
		logObject.Resp.RespBodyString = string(jsonResp)
		logString, _ := json.Marshal(logObject)
		loggerToDBObj.Data=string(logString)
		tools.WriteLogToDB(loggerToDBObj, r)
		//log.Println(string(logString))
		fmt.Fprint(w, string(jsonResp))
	}
}

func AuthMiddlewareForGetNotes(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	w.Header().Set("Content-Type", "application/json")
	var checkError ErrorHandlerArray
	token, err := request.ParseFromRequest(r, request.OAuth2Extractor, func(token *jwt.Token) (interface{}, error) {
		return publicKey, nil
	})
	if err == nil && token.Valid {
		next(w, r)
	} else {
		var loggerToDBObj tools.LoggerToDB
		url := fmt.Sprintf("%v", r.URL)
		loggerToDBObj.Method=r.Method
		loggerToDBObj.Path=url
		loggerToDBObj.IP=r.RemoteAddr
		loggerToDBObj.Status=strconv.Itoa(http.StatusUnauthorized)
		logObject := tools.FormatRequest(r)
		w.WriteHeader(http.StatusUnauthorized)
		checkError.ErrorMessage = "Token Failure"
		checkError.ErrorCode = 5
		jsonResp, _ := json.Marshal(checkError)
		logObject.Resp.RespBodyString = string(jsonResp)
		logString, _ := json.Marshal(logObject)
		loggerToDBObj.Data=string(logString)
		tools.WriteLogToDB(loggerToDBObj, r)
		//log.Println(string(logString))
		fmt.Fprint(w, string(jsonResp))
	}
}





func AddNote(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	token, err := request.ParseFromRequest(r, request.OAuth2Extractor, func(token *jwt.Token) (interface{}, error) {
		return publicKey, nil
	})
	claims := token.Claims.(jwt.MapClaims)
	var tempNote Note
	var checkError ErrorHandler
	var loggerToDBObj tools.LoggerToDB
	url := fmt.Sprintf("%v", r.URL)
	loggerToDBObj.Method=r.Method
	loggerToDBObj.Path=url
	loggerToDBObj.IP=r.RemoteAddr
	loggerToDBObj.Status=strconv.Itoa(http.StatusOK)
	logObject := tools.FormatRequest(r)
	if err = json.NewDecoder(r.Body).Decode(&tempNote); err != nil{
		loggerToDBObj.Status=strconv.Itoa(http.StatusBadRequest)
		w.WriteHeader(http.StatusBadRequest)
		checkError.ErrorCode=2
		checkError.ErrorMessage=err.Error()
		jsonResp,_ := json.Marshal(checkError)
		logObject.Resp.RespBodyString = string(jsonResp)
		logString, _ := json.Marshal(logObject)
		loggerToDBObj.Data=string(logString)
		tools.WriteLogToDB(loggerToDBObj, r)
		//log.Println(string(logString))
		fmt.Fprintf(w, string(jsonResp))
		return
	}
	if len(tempNote.Note) <= 0 || tempNote.ID != 0 || tempNote.UserID !=0 || len(tempNote.IsPublic) != 0 || !tempNote.CreatedAt.IsZero() || !tempNote.UpdatedAt.IsZero() || tempNote.DeletedAt != nil{
		loggerToDBObj.Status=strconv.Itoa(http.StatusBadRequest)
		w.WriteHeader(http.StatusBadRequest)
		checkError.ErrorMessage=MissingJSONErr.Error()
		checkError.ErrorCode=2
		jsonResp,_ := json.Marshal(checkError)
		logObject.Resp.RespBodyString = string(jsonResp)
		logString, _ := json.Marshal(logObject)
		loggerToDBObj.Data=string(logString)
		tools.WriteLogToDB(loggerToDBObj, r)
		//log.Println(string(logString))
		fmt.Fprintf(w, string(jsonResp))
		return
	}
	tempNote.IsPublic = "false"
	tempNote.UserID = uint64(claims["userID"].(float64))
	tx := tools.DB.Begin()
	if err := tx.Create(&tempNote).Error; err != nil{
		tx.Rollback()
		loggerToDBObj.Status=strconv.Itoa(http.StatusServiceUnavailable)
		w.WriteHeader(http.StatusServiceUnavailable)
		checkError.ErrorCode=3
		checkError.ErrorMessage=err.Error()
		jsonResp, _ := json.Marshal(checkError)
		logObject.Resp.RespBodyString = string(jsonResp)
		logString, _ := json.Marshal(logObject)
		loggerToDBObj.Data=string(logString)
		tools.WriteLogToDB(loggerToDBObj, r)
		//log.Println(string(logString))
		fmt.Fprintf(w, string(jsonResp))
		return
	}
	tx.Commit()

	checkError.Values = &Note{
		ID:        tempNote.ID,
		Note:      tempNote.Note,
		CreatedAt: tempNote.CreatedAt,
		UpdatedAt: tempNote.UpdatedAt,
	}

	checkError.ErrorCode=0
	checkError.ErrorMessage="success"
	jsonResp, err := json.Marshal(checkError)
	logObject.Resp.RespBodyString = string(jsonResp)
	logString, _ := json.Marshal(logObject)
	loggerToDBObj.Data=string(logString)
	tools.WriteLogToDB(loggerToDBObj, r)
	//log.Println(string(logString))
	fmt.Fprintf(w, string(jsonResp))
}

func GetNotes(w http.ResponseWriter, r *http.Request){
	w.Header().Set("Content-Type", "application/json")
	token, err := request.ParseFromRequest(r, request.OAuth2Extractor, func(token *jwt.Token) (interface{}, error) {
		return publicKey, nil
	})
	claims := token.Claims.(jwt.MapClaims)
	var (
		checkError ErrorHandlerArray
		loggerToDBObj tools.LoggerToDB
		createdAt time.Time
		updatedAt time.Time
		deletedAt *time.Time
		note string
		id uint64
		userid uint64
		isPublic string
		sharedUsers pq.StringArray
		tags	pq.StringArray
		getNote = Note{}
	)


	url := fmt.Sprintf("%v", r.URL)
	loggerToDBObj.Method=r.Method
	loggerToDBObj.Path=url
	loggerToDBObj.IP=r.RemoteAddr
	loggerToDBObj.Status=strconv.Itoa(http.StatusOK)
	logObject := tools.FormatRequest(r)
	rows, err := tools.DB.Table("notes").Rows()
	if err != nil {
		loggerToDBObj.Status=strconv.Itoa(http.StatusServiceUnavailable)
		w.WriteHeader(http.StatusServiceUnavailable)
		checkError.ErrorCode=3
		checkError.ErrorMessage=err.Error()
		jsonResp, _ := json.Marshal(checkError)
		logObject.Resp.RespBodyString = string(jsonResp)
		logString, _ := json.Marshal(logObject)
		loggerToDBObj.Data=string(logString)
		tools.WriteLogToDB(loggerToDBObj, r)
		//log.Println(string(logString))
		fmt.Fprintf(w, string(jsonResp))
		log.Fatal(err)
		return
	}

	checkError.ValueArray = make([]Note, 0)

	defer rows.Close()
	for rows.Next() {
		if err := rows.Scan(&id, &createdAt, &updatedAt, &deletedAt, &note, &userid, &isPublic, &sharedUsers, &tags); err != nil {
			loggerToDBObj.Status=strconv.Itoa(http.StatusServiceUnavailable)
			w.WriteHeader(http.StatusServiceUnavailable)
			checkError.ErrorCode=3
			checkError.ErrorMessage=err.Error()
			jsonResp, _ := json.Marshal(checkError)
			logObject.Resp.RespBodyString = string(jsonResp)
			logString, _ := json.Marshal(logObject)
			loggerToDBObj.Data=string(logString)
			tools.WriteLogToDB(loggerToDBObj, r)
			//log.Println(string(logString))
			fmt.Fprintf(w, string(jsonResp))
			log.Fatal(err)
		}

		getNote.ID = id
		getNote.Note = note
		getNote.CreatedAt = createdAt
		getNote.UpdatedAt = updatedAt
		getNote.IsPublic = isPublic
		getNote.SharedUsers = sharedUsers
		getNote.Tags = tags
		//getNote.UserID = userid


		tempoTemp := claims["userID"].(float64)
		tempUserID := uint64(tempoTemp)
		if tempUserID == userid && deletedAt == nil{
			checkError.ValueArray = append(checkError.ValueArray, getNote)
		}
	}
	checkError.ErrorCode=0
	checkError.ErrorMessage="success"
	jsonResp, _ := json.Marshal(checkError)
	fmt.Fprintf(w, string(jsonResp))
	/*if err := rows.Err(); err != nil {       ////////// What is this error check??
		loggerToDBObj.Status=strconv.Itoa(http.StatusServiceUnavailable)
		w.WriteHeader(http.StatusServiceUnavailable)
		checkError.ErrorCode=3
		checkError.ErrorMessage=err.Error()
		jsonResp, _ := json.Marshal(checkError)
		logObject.Resp.RespBodyString = string(jsonResp)
		logString, _ := json.Marshal(logObject)
		loggerToDBObj.Data=string(logString)
		tools.WriteLogToDB(loggerToDBObj, r)
		//log.Println(string(logString))
		fmt.Fprintf(w, string(jsonResp))
		log.Fatal(err)
	}*/
}

func UpdateNote(w http.ResponseWriter, r *http.Request){

	w.Header().Set("Content-Type", "application/json")
	var checkError ErrorHandler
	var loggerToDBObj tools.LoggerToDB
	url := fmt.Sprintf("%v", r.URL)
	loggerToDBObj.Method=r.Method
	loggerToDBObj.Path=url
	loggerToDBObj.IP=r.RemoteAddr
	loggerToDBObj.Status=strconv.Itoa(http.StatusOK)
	logObject := tools.FormatRequest(r)
	newID, err := strconv.ParseUint(path.Base(r.URL.Path),10,64)
	if err != nil {
		loggerToDBObj.Status=strconv.Itoa(http.StatusServiceUnavailable)
		w.WriteHeader(http.StatusServiceUnavailable)
		checkError.ErrorCode=6
		checkError.ErrorMessage=err.Error()
		jsonResp,_:=json.Marshal(checkError)
		logObject.Resp.RespBodyString = string(jsonResp)
		logString, _ := json.Marshal(logObject)
		loggerToDBObj.Data=string(logString)
		tools.WriteLogToDB(loggerToDBObj, r)
		//log.Println(string(logString))
		fmt.Fprintf(w,string(jsonResp))
		return
	}
	token, err := request.ParseFromRequest(r, request.OAuth2Extractor, func(token *jwt.Token) (interface{}, error) {
		return publicKey, nil
	})
	claims := token.Claims.(jwt.MapClaims)
	var newInput Note
	err = json.NewDecoder(r.Body).Decode(&newInput)
	if err != nil{
		loggerToDBObj.Status=strconv.Itoa(http.StatusBadRequest)
		w.WriteHeader(http.StatusBadRequest)
		checkError.ErrorCode=2
		checkError.ErrorMessage=err.Error()
		jsonResp,_ := json.Marshal(checkError)
		logObject.Resp.RespBodyString = string(jsonResp)
		logString, _ := json.Marshal(logObject)
		loggerToDBObj.Data=string(logString)
		tools.WriteLogToDB(loggerToDBObj, r)
		//log.Println(string(logString))
		fmt.Fprintf(w, string(jsonResp))
		return
	}
	if len(newInput.Note) <= 0 || newInput.ID != 0 || newInput.UserID !=0{
		loggerToDBObj.Status=strconv.Itoa(http.StatusBadRequest)
		w.WriteHeader(http.StatusBadRequest)
		checkError.ErrorMessage=MissingJSONErr.Error()
		checkError.ErrorCode=2
		jsonResp,_ := json.Marshal(checkError)
		logObject.Resp.RespBodyString = string(jsonResp)
		logString, _ := json.Marshal(logObject)
		loggerToDBObj.Data=string(logString)
		tools.WriteLogToDB(loggerToDBObj, r)
		//log.Println(string(logString))
		fmt.Fprintf(w, string(jsonResp))
		return
	}
	if newInput.UserID != 0{
		loggerToDBObj.Status=strconv.Itoa(http.StatusBadRequest)
		w.WriteHeader(http.StatusBadRequest)
		checkError.ErrorMessage="Invalid JSON"
		checkError.ErrorCode=2
		jsonResp,_ := json.Marshal(checkError)
		logObject.Resp.RespBodyString = string(jsonResp)
		logString, _ := json.Marshal(logObject)
		loggerToDBObj.Data=string(logString)
		tools.WriteLogToDB(loggerToDBObj, r)
		//log.Println(string(logString))
		fmt.Fprintf(w, string(jsonResp))
		return
	}
	newInput.ID = newID
	temp := Note{
		ID: newID,
	}
	if err := tools.DB.First(&temp, newID).Error; err != nil || temp.UserID != uint64(claims["userID"].(float64)){
		loggerToDBObj.Status=strconv.Itoa(http.StatusBadRequest)
		w.WriteHeader(http.StatusBadRequest)
		checkError.ErrorMessage=InvalidErr.Error()
		checkError.ErrorCode=1
		jsonResp,_ := json.Marshal(checkError)
		logObject.Resp.RespBodyString = string(jsonResp)
		logString, _ := json.Marshal(logObject)
		loggerToDBObj.Data=string(logString)
		tools.WriteLogToDB(loggerToDBObj, r)
		//log.Println(string(logString))
		fmt.Fprintf(w, string(jsonResp))
		return
	}
	tools.DB.Model(&newInput).Update("note", newInput.Note)
	checkError.ErrorCode=0
	checkError.ErrorMessage="success"
	checkError.Values = &Note{
		CreatedAt:temp.CreatedAt,
		UpdatedAt:newInput.UpdatedAt,
		//IsPublic:newInput.IsPublic,
		//SharedUsers:newInput.SharedUsers,
		//Tags:newInput	.Tags,
		ID:newID,
		Note:newInput.Note,
	}
	jsonResp,_ := json.Marshal(checkError)
	logObject.Resp.RespBodyString = string(jsonResp)
	logString, _ := json.Marshal(logObject)
	loggerToDBObj.Data=string(logString)
	tools.WriteLogToDB(loggerToDBObj, r)
	//log.Println(string(logString))
	fmt.Fprintf(w, string(jsonResp))
}

func DeleteNote(w http.ResponseWriter, r *http.Request){
	w.Header().Set("Content-Type", "application/json")
	//log.Println(string(logString))
	var newInput Note
	var checkError ErrorHandler
	var loggerToDBObj tools.LoggerToDB
	url := fmt.Sprintf("%v", r.URL)
	loggerToDBObj.Method=r.Method
	loggerToDBObj.Path=url
	loggerToDBObj.IP=r.RemoteAddr
	loggerToDBObj.Status=strconv.Itoa(http.StatusOK)
	logObject := tools.FormatRequest(r)
	newID, err := strconv.ParseUint(path.Base(r.URL.Path),10,64)
	if err != nil {
		checkError.ErrorCode=6
		checkError.ErrorMessage=err.Error()
		loggerToDBObj.Status=strconv.Itoa(http.StatusBadRequest)
		w.WriteHeader(http.StatusBadRequest)
		jsonResp, _ := json.Marshal(checkError)
		logObject.Resp.RespBodyString = string(jsonResp)
		logString, _ := json.Marshal(logObject)
		loggerToDBObj.Data=string(logString)
		tools.WriteLogToDB(loggerToDBObj, r)
		fmt.Fprintf(w, string(jsonResp))
		return
	}
	token, err := request.ParseFromRequest(r, request.OAuth2Extractor, func(token *jwt.Token) (interface{}, error) {
		return publicKey, nil
	})
	claims := token.Claims.(jwt.MapClaims)
	newInput.ID = newID
	temp := Note{
		ID:newID,
	}

	if err := tools.DB.First(&temp, newID).Error; err != nil || temp.UserID != uint64(claims["userID"].(float64)){
		loggerToDBObj.Status=strconv.Itoa(http.StatusBadRequest)
		w.WriteHeader(http.StatusBadRequest)
		checkError.ErrorMessage=InvalidErr.Error()
		checkError.ErrorCode=1
		jsonResp,_ := json.Marshal(checkError)
		logObject.Resp.RespBodyString = string(jsonResp)
		logString, _ := json.Marshal(logObject)
		loggerToDBObj.Data=string(logString)
		tools.WriteLogToDB(loggerToDBObj, r)
		//log.Println(string(logString))
		fmt.Fprintf(w, string(jsonResp))
		return
	}

	tools.DB.Delete(&newInput)

	checkError.ErrorCode=0
	checkError.ErrorMessage="success"
	checkError.Values = &Note{
		ID:newID,
		Note:temp.Note,
		CreatedAt:temp.CreatedAt,
		UpdatedAt:temp.UpdatedAt,
	}
	jsonResp,_ := json.Marshal(checkError)
	logObject.Resp.RespBodyString = string(jsonResp)
	logString, _ := json.Marshal(logObject)
	loggerToDBObj.Data=string(logString)
	tools.WriteLogToDB(loggerToDBObj, r)
	//log.Println(string(logString))
	fmt.Fprintf(w, string(jsonResp))
}

func GetSingleNote(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	token, err := request.ParseFromRequest(r, request.OAuth2Extractor, func(token *jwt.Token) (interface{}, error) {
		return publicKey, nil
	})

	logObject := tools.FormatRequest(r)
	claims := token.Claims.(jwt.MapClaims)
	var (
		//note string
		//id uint64
		//userid uint64
		checkError ErrorHandler
		//getNote Note
	)
	var loggerToDBObj tools.LoggerToDB
	url := fmt.Sprintf("%v", r.URL)
	loggerToDBObj.Method=r.Method
	loggerToDBObj.Path=url
	loggerToDBObj.IP=r.RemoteAddr
	loggerToDBObj.Status=strconv.Itoa(http.StatusOK)

	newID, err := strconv.ParseUint(path.Base(r.URL.Path),10,64)
	if err != nil {
		loggerToDBObj.Status=strconv.Itoa(http.StatusBadRequest)
		w.WriteHeader(http.StatusBadRequest)
		jsonResp, _ := json.Marshal(err.Error())
		logObject.Resp.RespBodyString = string(jsonResp)
		logString, _ := json.Marshal(logObject)
		loggerToDBObj.Data=string(logString)
		tools.WriteLogToDB(loggerToDBObj, r)
		//log.Println(string(logString))
		fmt.Fprintf(w, string(jsonResp))
		return
	}
	temp := Note{
		ID:newID,
	}
	if err := tools.DB.First(&temp, newID).Error; err != nil || temp.UserID != uint64(claims["userID"].(float64)){
		loggerToDBObj.Status=strconv.Itoa(http.StatusBadRequest)
		w.WriteHeader(http.StatusBadRequest)
		checkError.ErrorMessage=InvalidErr.Error()
		checkError.ErrorCode=1
		jsonResp,_ := json.Marshal(checkError)
		logObject.Resp.RespBodyString = string(jsonResp)
		logString, _ := json.Marshal(logObject)
		loggerToDBObj.Data=string(logString)
		tools.WriteLogToDB(loggerToDBObj, r)
		//log.Println(string(logString))
		fmt.Fprintf(w, string(jsonResp))
		return
	}
	loggerToDBObj.Status=strconv.Itoa(http.StatusOK)
	w.WriteHeader(http.StatusOK)
	checkError.ErrorMessage="success"
	checkError.ErrorCode=0
	checkError.Values = &temp
	jsonResp,_ := json.Marshal(checkError)
	logObject.Resp.RespBodyString = string(jsonResp)
	logString, _ := json.Marshal(logObject)
	loggerToDBObj.Data=string(logString)
	tools.WriteLogToDB(loggerToDBObj, r)
	//log.Println(string(logString))
	fmt.Fprintf(w, string(jsonResp))
}

func RegisterFunc(w http.ResponseWriter, r *http.Request){
	w.Header().Set("Content-Type", "application/json")
	var (
		requestingUser User
		checkError ErrorHandler
		loggerToDBObj tools.LoggerToDB
	)
	url := fmt.Sprintf("%v", r.URL)
	loggerToDBObj.Method=r.Method
	loggerToDBObj.Path=url
	loggerToDBObj.IP=r.RemoteAddr
	logObject := tools.FormatRequest(r)
	if err = json.NewDecoder(r.Body).Decode(&requestingUser); err != nil{
		w.WriteHeader(http.StatusBadRequest)
		loggerToDBObj.Status=strconv.Itoa(http.StatusBadRequest)
		checkError.ErrorCode=2
		checkError.ErrorMessage=err.Error()
		jsonResp,_ := json.Marshal(checkError)
		logObject.Resp.RespBodyString = string(jsonResp)
		logString, _ := json.Marshal(logObject)
		loggerToDBObj.Data=string(logString)
		tools.WriteLogToDB(loggerToDBObj, r)
		//log.Println(string(logString))
		fmt.Fprintf(w, string(jsonResp))
		return
	}
	if requestingUser.UserID != 0{
		w.WriteHeader(http.StatusBadRequest)
		loggerToDBObj.Status=strconv.Itoa(http.StatusBadRequest)
		checkError.ErrorMessage="invalid json"
		checkError.ErrorCode=2
		jsonResp,_ := json.Marshal(checkError)
		logObject.Resp.RespBodyString = string(jsonResp)
		logString, _ := json.Marshal(logObject)
		loggerToDBObj.Data=string(logString)
		tools.WriteLogToDB(loggerToDBObj, r)
		//log.Println(string(logString))
		fmt.Fprintf(w, string(jsonResp))
		return
	}
	if len(requestingUser.Username) <= 0 || len(requestingUser.Password) <= 0{
		w.WriteHeader(http.StatusBadRequest)
		loggerToDBObj.Status=strconv.Itoa(http.StatusBadRequest)
		checkError.ErrorMessage="Username or Password cannot be empty"
		checkError.ErrorCode=2
		jsonResp,_ := json.Marshal(checkError)
		logObject.Resp.RespBodyString = string(jsonResp)
		logString, _ := json.Marshal(logObject)
		loggerToDBObj.Data=string(logString)
		tools.WriteLogToDB(loggerToDBObj, r)
		//log.Println(string(logString))
		fmt.Fprintf(w, string(jsonResp))
		return
	}
	if !tools.ValidateEmail(requestingUser.Username){
		loggerToDBObj.Status=strconv.Itoa(http.StatusBadRequest)
		w.WriteHeader(http.StatusBadRequest)
		checkError.ErrorMessage="This is not a valid email address"
		checkError.ErrorCode=3
		jsonResp,_ := json.Marshal(checkError)
		logObject.Resp.RespBodyString = string(jsonResp)
		logString, _ := json.Marshal(logObject)
		loggerToDBObj.Data=string(logString)
		tools.WriteLogToDB(loggerToDBObj, r)
		//log.Println(string(logString))
		fmt.Fprintf(w, string(jsonResp))
		return
	}
	if err := tools.DB.Where("username = ?", requestingUser.Username).First(&requestingUser).Error; err == nil{
		loggerToDBObj.Status=strconv.Itoa(http.StatusBadRequest)
		w.WriteHeader(http.StatusBadRequest)
		checkError.ErrorMessage="This username already exists"
		checkError.ErrorCode=3
		jsonResp,_ := json.Marshal(checkError)
		logObject.Resp.RespBodyString = string(jsonResp)
		logString, _ := json.Marshal(logObject)
		loggerToDBObj.Data=string(logString)
		tools.WriteLogToDB(loggerToDBObj, r)
		//log.Println(string(logString))
		fmt.Fprintf(w, string(jsonResp))
		return
	}
	log.Println(requestingUser.Username)
	tx := tools.DB.Begin().Table("users")
	//err := tools.DB.Create(&t)
	if err := tx.Create(&requestingUser).Error; err != nil{
		tx.Rollback()
		loggerToDBObj.Status=strconv.Itoa(http.StatusServiceUnavailable)
		w.WriteHeader(http.StatusServiceUnavailable)
		checkError.ErrorCode=3
		checkError.ErrorMessage=err.Error()
		jsonResp, _ := json.Marshal(checkError)
		logObject.Resp.RespBodyString = string(jsonResp)
		logString, _ := json.Marshal(logObject)
		loggerToDBObj.Data=string(logString)
		tools.WriteLogToDB(loggerToDBObj, r)
		//log.Println(string(logString))
		fmt.Fprintf(w, string(jsonResp))
		return
	}
	tx.Commit()
	loggerToDBObj.Status=strconv.Itoa(http.StatusOK)
	checkError.ErrorCode=0
	checkError.ErrorMessage="success"
	jsonResp, _ := json.Marshal(checkError)
	logObject.Resp.RespBodyString = string(jsonResp)
	logString, _ := json.Marshal(logObject)
	loggerToDBObj.Data=string(logString)
	tools.WriteLogToDB(loggerToDBObj, r)
	//log.Println(string(logString))
	fmt.Fprintf(w, string(jsonResp))
}


func ChangePasswordRequest(w http.ResponseWriter, r *http.Request){
	w.Header().Set("Content-Type", "application/json")
	var (
		checkError ErrorHandler
		requestingUser	User
		userToken uint64
		pwRequest changePwRequest
		userName string
		passWord string
		userid uint64
		loggerToDBObj tools.LoggerToDB
		createdAt time.Time
		updatedAt time.Time
		deletedAt *time.Time
	)
	url := fmt.Sprintf("%v", r.URL)
	loggerToDBObj.Method=r.Method
	loggerToDBObj.Path=url
	loggerToDBObj.IP=r.RemoteAddr
	loggerToDBObj.Status=strconv.Itoa(http.StatusOK)
	logObject := tools.FormatRequest(r)
	rows, err := tools.DB.Table("users").Rows()
	if err != nil {
		w.WriteHeader(http.StatusServiceUnavailable)
		loggerToDBObj.Status=strconv.Itoa(http.StatusServiceUnavailable)
		checkError.ErrorCode=3
		checkError.ErrorMessage=err.Error()
		jsonResp, _ := json.Marshal(checkError)
		logObject.Resp.RespBodyString = string(jsonResp)
		logString, _ := json.Marshal(logObject)
		loggerToDBObj.Data=string(logString)
		tools.WriteLogToDB(loggerToDBObj, r)
		//log.Println(string(logString))
		fmt.Fprintf(w, string(jsonResp))
		log.Fatal(err)
		return
	}
	if err = json.NewDecoder(r.Body).Decode(&requestingUser); err != nil{
		w.WriteHeader(http.StatusBadRequest)
		loggerToDBObj.Status=strconv.Itoa(http.StatusBadRequest)
		checkError.ErrorCode=2
		checkError.ErrorMessage=err.Error()
		jsonResp,_ := json.Marshal(checkError)
		logObject.Resp.RespBodyString = string(jsonResp)
		logString, _ := json.Marshal(logObject)
		loggerToDBObj.Data=string(logString)
		tools.WriteLogToDB(loggerToDBObj, r)
		//log.Println(string(logString))
		fmt.Fprintf(w, string(jsonResp))
		return
	}
	if len(requestingUser.Username) <=0 || len(requestingUser.Password) != 0 || requestingUser.UserID !=0  {
		w.WriteHeader(http.StatusBadRequest)
		loggerToDBObj.Status=strconv.Itoa(http.StatusBadRequest)
		checkError.ErrorMessage="invalid json"
		checkError.ErrorCode=2
		jsonResp,_ := json.Marshal(checkError)
		logObject.Resp.RespBodyString = string(jsonResp)
		logString, _ := json.Marshal(logObject)
		loggerToDBObj.Data=string(logString)
		tools.WriteLogToDB(loggerToDBObj, r)
		//log.Println(string(logString))
		fmt.Fprintf(w, string(jsonResp))
		return
	}
	defer rows.Close()
	for rows.Next() {
		if err := rows.Scan(&userid, &createdAt, &updatedAt, &deletedAt, &userName, &passWord); err != nil {
			w.WriteHeader(http.StatusServiceUnavailable)
			loggerToDBObj.Status=strconv.Itoa(http.StatusServiceUnavailable)
			checkError.ErrorCode=3
			checkError.ErrorMessage=err.Error()
			jsonResp, _ := json.Marshal(checkError)
			logObject.Resp.RespBodyString = string(jsonResp)
			logString, _ := json.Marshal(logObject)
			loggerToDBObj.Data=string(logString)
			tools.WriteLogToDB(loggerToDBObj, r)
			//log.Println(string(logString))
			fmt.Fprintf(w, string(jsonResp))
			log.Fatal(err)
		}
		if requestingUser.Username == userName{
			var checkLoginError LoginErrorHandler
			randSource := rand.NewSource(time.Now().UnixNano())
			randN := rand.New(randSource)
			otp := randN.Intn(899999)
			otp = otp + 100000
			pwRequest.OTP = otp
			pwRequest.UserID = userid
			tx := tools.DB.Begin().Table("changepwrequests")
			//err := tools.DB.Create(&t)
			if err := tx.Create(&pwRequest).Error; err != nil{
				tx.Rollback()
				loggerToDBObj.Status=strconv.Itoa(http.StatusServiceUnavailable)
				w.WriteHeader(http.StatusServiceUnavailable)
				checkError.ErrorCode=3
				checkError.ErrorMessage=err.Error()
				jsonResp, _ := json.Marshal(checkError)
				logObject.Resp.RespBodyString = string(jsonResp)
				logString, _ := json.Marshal(logObject)
				loggerToDBObj.Data=string(logString)
				tools.WriteLogToDB(loggerToDBObj, r)
				//log.Println(string(logString))
				fmt.Fprintf(w, string(jsonResp))
				return
			}
			tx.Commit()
			pinString := strconv.Itoa(otp)
			from := mail.NewEmail("Orange Notes", "support@orangenotes.com")
			subject := "Your Reset Password Request"
			to := mail.NewEmail("Requester", requestingUser.Username)
			plainTextContent := "Your Pin is " + pinString
			htmlContent := "Your Pin is " + pinString
			message := mail.NewSingleEmail(from, subject, to, plainTextContent, htmlContent)
			client := sendgrid.NewSendClient(os.Getenv("SENDGRID_API_KEY"))
			response, err := client.Send(message)
			if err != nil {
				log.Println(err)
			} else {
				log.Println(response.StatusCode)
				log.Println(response.Body)
				log.Println(response.Headers)
			}
			userToken = userid
			token := jwt.New(jwt.GetSigningMethod("RS256"))
			claims := token.Claims.(jwt.MapClaims)
			claims["userID"] = userToken
			claims["exp"] = time.Now().Add(time.Minute * 3).Unix()
			tokenString, _ := token.SignedString(privateKey)
			checkLoginError.ErrorCode=0
			checkLoginError.ErrorMessage="success"
			checkLoginError.Context.JwtToken=tokenString
			jsonResp,_ := json.Marshal(checkLoginError)
			logObject.Resp.RespBodyString = string(jsonResp)
			logString, _ := json.Marshal(logObject)
			loggerToDBObj.Data=string(logString)
			tools.WriteLogToDB(loggerToDBObj, r)
			//log.Println(string(logString))
			fmt.Fprintf(w, string(jsonResp))
			return
		}
	}
	checkError.ErrorMessage = "this username does not exist"
	checkError.ErrorCode = 3
	w.WriteHeader(http.StatusBadRequest)
	loggerToDBObj.Status=strconv.Itoa(http.StatusBadRequest)
	jsonResp, _ := json.Marshal(checkError)
	logObject.Resp.RespBodyString = string(jsonResp)
	logString, _ := json.Marshal(logObject)
	loggerToDBObj.Data=string(logString)
	tools.WriteLogToDB(loggerToDBObj, r)
	fmt.Fprintf(w, string(jsonResp))
}

func ChangePasswordConfirmation(w http.ResponseWriter, r *http.Request){
	token, err := request.ParseFromRequest(r, request.OAuth2Extractor, func(token *jwt.Token) (interface{}, error) {
		return publicKey, nil
	})
	var checkError ErrorHandler
	var loggerToDBObj tools.LoggerToDB
	var changeRequest changePwConfirm
	url := fmt.Sprintf("%v", r.URL)
	loggerToDBObj.Method=r.Method
	loggerToDBObj.Path=url
	loggerToDBObj.IP=r.RemoteAddr
	loggerToDBObj.Status=strconv.Itoa(http.StatusOK)
	logObject := tools.FormatRequest(r)
	claims := token.Claims.(jwt.MapClaims)
	if err = json.NewDecoder(r.Body).Decode(&changeRequest); err != nil{
		w.WriteHeader(http.StatusBadRequest)
		loggerToDBObj.Status=strconv.Itoa(http.StatusBadRequest)
		checkError.ErrorCode=2
		checkError.ErrorMessage=err.Error()
		jsonResp,_ := json.Marshal(checkError)
		logObject.Resp.RespBodyString = string(jsonResp)
		logString, _ := json.Marshal(logObject)
		loggerToDBObj.Data=string(logString)
		tools.WriteLogToDB(loggerToDBObj, r)
		//log.Println(string(logString))
		fmt.Fprintf(w, string(jsonResp))
		return
	}
	if len(changeRequest.RequestingUsername) <= 0 || len(changeRequest.NewPassword) <= 0 || changeRequest.OTP <= 0{
		w.WriteHeader(http.StatusBadRequest)
		loggerToDBObj.Status=strconv.Itoa(http.StatusBadRequest)
		checkError.ErrorCode=2
		checkError.ErrorMessage=err.Error()
		jsonResp,_ := json.Marshal(checkError)
		logObject.Resp.RespBodyString = string(jsonResp)
		logString, _ := json.Marshal(logObject)
		loggerToDBObj.Data=string(logString)
		tools.WriteLogToDB(loggerToDBObj, r)
		//log.Println(string(logString))
		fmt.Fprintf(w, string(jsonResp))
		return
	}
	temp := &User{
		UserID: uint64(claims["userID"].(float64)),
	}
	if err := tools.DB.First(&temp).Error; err != nil{
		loggerToDBObj.Status=strconv.Itoa(http.StatusBadRequest)
		w.WriteHeader(http.StatusBadRequest)
		checkError.ErrorMessage=err.Error()
		checkError.ErrorCode=1
		jsonResp,_ := json.Marshal(checkError)
		logObject.Resp.RespBodyString = string(jsonResp)
		logString, _ := json.Marshal(logObject)
		loggerToDBObj.Data=string(logString)
		tools.WriteLogToDB(loggerToDBObj, r)
		//log.Println(string(logString))
		fmt.Fprintf(w, string(jsonResp))
		return
	}
	if changeRequest.RequestingUsername != temp.Username{
		w.WriteHeader(http.StatusUnauthorized)
		loggerToDBObj.Status=strconv.Itoa(http.StatusUnauthorized)
		checkError.ErrorMessage="unauthorized request"
		checkError.ErrorCode=7
		jsonResp,_ := json.Marshal(checkError)
		logObject.Resp.RespBodyString = string(jsonResp)
		logString, _ := json.Marshal(logObject)
		loggerToDBObj.Data=string(logString)
		tools.WriteLogToDB(loggerToDBObj, r)
		//log.Println(string(logString))
		fmt.Fprintf(w, string(jsonResp))
		return
	}
	rows, err := tools.DB.Table("changepwrequests").Rows()
	if err != nil {
		w.WriteHeader(http.StatusServiceUnavailable)
		loggerToDBObj.Status=strconv.Itoa(http.StatusServiceUnavailable)
		checkError.ErrorCode=3
		checkError.ErrorMessage=err.Error()
		jsonResp, _ := json.Marshal(checkError)
		logObject.Resp.RespBodyString = string(jsonResp)
		logString, _ := json.Marshal(logObject)
		loggerToDBObj.Data=string(logString)
		tools.WriteLogToDB(loggerToDBObj, r)
		//log.Println(string(logString))
		fmt.Fprintf(w, string(jsonResp))
		log.Fatal(err)
		return
	}
	var (
		userid uint64
		createdAt time.Time
		updatedAt time.Time
		deletedAt *time.Time
		id uint64
		otp int
	)
	defer rows.Close()
	for rows.Next(){
		if err := rows.Scan(&id, &createdAt, &updatedAt, &deletedAt, &userid, &otp); err != nil {
			w.WriteHeader(http.StatusServiceUnavailable)
			loggerToDBObj.Status=strconv.Itoa(http.StatusServiceUnavailable)
			checkError.ErrorCode=3
			checkError.ErrorMessage=err.Error()
			jsonResp, _ := json.Marshal(checkError)
			logObject.Resp.RespBodyString = string(jsonResp)
			logString, _ := json.Marshal(logObject)
			loggerToDBObj.Data=string(logString)
			tools.WriteLogToDB(loggerToDBObj, r)
			//log.Println(string(logString))
			fmt.Fprintf(w, string(jsonResp))
			log.Fatal(err)
		}
		if changeRequest.OTP == otp && uint64(claims["userID"].(float64)) == userid && time.Since(createdAt).Minutes() <= 3{
			temp.Password = changeRequest.NewPassword
			tools.DB.Save(&temp)
			checkError.ErrorCode=0
			checkError.ErrorMessage="success"
			jsonResp,_ := json.Marshal(checkError)
			logObject.Resp.RespBodyString = string(jsonResp)
			logString, _ := json.Marshal(logObject)
			loggerToDBObj.Data=string(logString)
			tools.WriteLogToDB(loggerToDBObj, r)
			//log.Println(string(logString))
			fmt.Fprintf(w, string(jsonResp))
			return
		}

	}
}

func SetOptions(w http.ResponseWriter, r *http.Request){
	var (
		checkError    ErrorHandler
		loggerToDBObj tools.LoggerToDB
		requestedNote Note
		tempNote Note
	)
	url := fmt.Sprintf("%v", r.URL)
	loggerToDBObj.Method=r.Method
	loggerToDBObj.Path=url
	loggerToDBObj.IP=r.RemoteAddr
	loggerToDBObj.Status=strconv.Itoa(http.StatusOK)
	logObject := tools.FormatRequest(r)
	requestedNoteID, err := strconv.ParseUint(path.Base(r.URL.Path),10,64)
	if err != nil {
		loggerToDBObj.Status=strconv.Itoa(http.StatusBadRequest)
		w.WriteHeader(http.StatusBadRequest)
		checkError.ErrorCode=6
		checkError.ErrorMessage=err.Error()
		jsonResp,_:=json.Marshal(checkError)
		logObject.Resp.RespBodyString = string(jsonResp)
		logString, _ := json.Marshal(logObject)
		loggerToDBObj.Data=string(logString)
		tools.WriteLogToDB(loggerToDBObj, r)
		//log.Println(string(logString))
		fmt.Fprintf(w,string(jsonResp))
		return
	}
	token, _ := request.ParseFromRequest(r, request.OAuth2Extractor, func(token *jwt.Token) (interface{}, error) {
		return publicKey, nil
	})
	claims := token.Claims.(jwt.MapClaims)
	if err = json.NewDecoder(r.Body).Decode(&tempNote); err != nil{
		w.WriteHeader(http.StatusBadRequest)
		loggerToDBObj.Status=strconv.Itoa(http.StatusBadRequest)
		checkError.ErrorCode=2
		checkError.ErrorMessage=err.Error()
		jsonResp,_ := json.Marshal(checkError)
		logObject.Resp.RespBodyString = string(jsonResp)
		logString, _ := json.Marshal(logObject)
		loggerToDBObj.Data=string(logString)
		tools.WriteLogToDB(loggerToDBObj, r)
		//log.Println(string(logString))
		fmt.Fprintf(w, string(jsonResp))
		return
	}

	if len(tempNote.Note) != 0 || tempNote.UserID != 0 || tempNote.ID != 0 || !tempNote.CreatedAt.IsZero() || !tempNote.UpdatedAt.IsZero() || tempNote.DeletedAt != nil{
		w.WriteHeader(http.StatusUnauthorized)
		loggerToDBObj.Status=strconv.Itoa(http.StatusUnauthorized)
		checkError.ErrorCode=2
		checkError.ErrorMessage="unauthorized action"
		jsonResp,_ := json.Marshal(checkError)
		logObject.Resp.RespBodyString = string(jsonResp)
		logString, _ := json.Marshal(logObject)
		loggerToDBObj.Data=string(logString)
		tools.WriteLogToDB(loggerToDBObj, r)
		//log.Println(string(logString))
		fmt.Fprintf(w, string(jsonResp))
		return
	}
	if err := tools.DB.Where("id = ?", requestedNoteID).First(&requestedNote).Error; err != nil{
		loggerToDBObj.Status=strconv.Itoa(http.StatusBadRequest)
		w.WriteHeader(http.StatusBadRequest)
		checkError.ErrorMessage=err.Error()
		checkError.ErrorCode=1
		jsonResp,_ := json.Marshal(checkError)
		logObject.Resp.RespBodyString = string(jsonResp)
		logString, _ := json.Marshal(logObject)
		loggerToDBObj.Data=string(logString)
		tools.WriteLogToDB(loggerToDBObj, r)
		//log.Println(string(logString))
		fmt.Fprintf(w, string(jsonResp))
		return
	}
	if uint64(claims["userID"].(float64)) != requestedNote.UserID{
		w.WriteHeader(http.StatusUnauthorized)
		loggerToDBObj.Status=strconv.Itoa(http.StatusUnauthorized)
		log.Println(requestedNote.UserID)
		log.Println(claims["userID"])
		log.Println(uint64(claims["userID"].(float64)))
		checkError.ErrorCode=2
		checkError.ErrorMessage="unauthorized action"
		jsonResp,_ := json.Marshal(checkError)
		logObject.Resp.RespBodyString = string(jsonResp)
		logString, _ := json.Marshal(logObject)
		loggerToDBObj.Data=string(logString)
		tools.WriteLogToDB(loggerToDBObj, r)
		//log.Println(string(logString))
		fmt.Fprintf(w, string(jsonResp))
		return
	}
	if tempNote.IsPublic == "true" || tempNote.IsPublic == "false" {
		requestedNote.IsPublic = tempNote.IsPublic
	}
	requestedNote.Tags = tempNote.Tags // append(requestedNote.Tags, tempNote.Tags...)
	if len(tempNote.SharedUsers) > 0{ // for email validation | greater than 0, because it can be empty
		i := 0
		for range tempNote.SharedUsers{
			if !tools.ValidateEmail(tempNote.SharedUsers[i]){
				checkError.ErrorCode=3
				checkError.ErrorMessage="invalid email address"
				jsonResp,_ := json.Marshal(checkError)
				logObject.Resp.RespBodyString = string(jsonResp)
				logString, _ := json.Marshal(logObject)
				loggerToDBObj.Data=string(logString)
				tools.WriteLogToDB(loggerToDBObj, r)
				//log.Println(string(logString))
				fmt.Fprintf(w, string(jsonResp))
				return
			}
			i++
		}
		requestedNote.SharedUsers = tempNote.SharedUsers //append(requestedNote.SharedUsers, tempNote.SharedUsers...)
	}else{  //if user wants to delete every sharedUser
		requestedNote.SharedUsers = tempNote.SharedUsers
	}
	tools.DB.Save(&requestedNote)
	w.WriteHeader(http.StatusOK)
	loggerToDBObj.Status=strconv.Itoa(http.StatusOK)
	checkError.ErrorCode=0
	checkError.ErrorMessage="success"
	checkError.Values = &requestedNote
	jsonResp,_ := json.Marshal(checkError)
	logObject.Resp.RespBodyString = string(jsonResp)
	logString, _ := json.Marshal(logObject)
	loggerToDBObj.Data=string(logString)
	tools.WriteLogToDB(loggerToDBObj, r)
	//log.Println(string(logString))
	fmt.Fprintf(w, string(jsonResp))
	return
}

func LoginFunc(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	//var logString string
	//logString = "Time=" + time.Now().String() + " | Remote IP="  + r.RemoteAddr + " | Status=" + strconv.Itoa(http.StatusOK) + " | Path= /note/login | Method=" + r.Method
	//log.Println(logString)
	//defer r.Body.Close()

	var (
		userInput User
		userToken uint64
		checkError ErrorHandler
		loggerToDBObj tools.LoggerToDB
		requestingUser	User
	)

	url := fmt.Sprintf("%v", r.URL)
	loggerToDBObj.Method=r.Method
	loggerToDBObj.Path=url
	loggerToDBObj.IP=r.RemoteAddr
	loggerToDBObj.Status=strconv.Itoa(http.StatusOK)
	logObject := tools.FormatRequest(r)
	if err = json.NewDecoder(r.Body).Decode(&userInput); err != nil{
		w.WriteHeader(http.StatusBadRequest)
		loggerToDBObj.Status=strconv.Itoa(http.StatusBadRequest)
		checkError.ErrorCode=2
		checkError.ErrorMessage=err.Error()
		jsonResp,_ := json.Marshal(checkError)
		logObject.Resp.RespBodyString = string(jsonResp)
		logString, _ := json.Marshal(logObject)
		loggerToDBObj.Data=string(logString)
		tools.WriteLogToDB(loggerToDBObj, r)
		fmt.Fprintf(w, string(jsonResp))
		return
	}
	if len(userInput.Username) <=0 || len(userInput.Password) <= 0{
		w.WriteHeader(http.StatusBadRequest)
		loggerToDBObj.Status=strconv.Itoa(http.StatusBadRequest)
		checkError.ErrorMessage="Username or Password cannot be empty"
		checkError.ErrorCode=2
		jsonResp,_ := json.Marshal(checkError)
		logObject.Resp.RespBodyString = string(jsonResp)
		logString, _ := json.Marshal(logObject)
		loggerToDBObj.Data=string(logString)
		tools.WriteLogToDB(loggerToDBObj, r)
		fmt.Fprintf(w, string(jsonResp))
		return
	}
	if err := tools.DB.Where("username = ?", userInput.Username).First(&requestingUser).Error; err != nil{
		loggerToDBObj.Status=strconv.Itoa(http.StatusBadRequest)
		w.WriteHeader(http.StatusBadRequest)
		log.Println(requestingUser.Username)
		log.Println(userInput.Username)
		checkError.ErrorMessage="This username does not exists"
		checkError.ErrorCode=3
		jsonResp,_ := json.Marshal(checkError)
		logObject.Resp.RespBodyString = string(jsonResp)
		logString, _ := json.Marshal(logObject)
		loggerToDBObj.Data=string(logString)
		tools.WriteLogToDB(loggerToDBObj, r)
		//log.Println(string(logString))
		fmt.Fprintf(w, string(jsonResp))
		return
	}

	if requestingUser.Username == userInput.Username && userInput.Password == requestingUser.Password{
		var checkLoginError LoginErrorHandler
		userToken = requestingUser.UserID
		token := jwt.New(jwt.GetSigningMethod("RS256"))
		claims := token.Claims.(jwt.MapClaims)
		claims["userID"] = userToken
		claims["exp"] = time.Now().Add(time.Hour * 24).Unix()
		tokenString, _ := token.SignedString(privateKey)
		checkLoginError.ErrorCode=0
		checkLoginError.ErrorMessage="success"
		checkLoginError.Context.JwtToken=tokenString
		jsonResp,_ := json.Marshal(checkLoginError)
		logObject.Resp.RespBodyString = string(jsonResp)
		logString, _ := json.Marshal(logObject)
		loggerToDBObj.Data=string(logString)
		tools.WriteLogToDB(loggerToDBObj, r)
		//log.Println(string(logString))
		fmt.Fprintf(w, string(jsonResp))
		return
	}
	checkError.ErrorMessage = "Username and Password do not match"
	checkError.ErrorCode = 3
	w.WriteHeader(http.StatusBadRequest)
	loggerToDBObj.Status=strconv.Itoa(http.StatusBadGateway)
	jsonResp, _ := json.Marshal(checkError)
	logObject.Resp.RespBodyString = string(jsonResp)
	logString, _ := json.Marshal(logObject)
	loggerToDBObj.Data=string(logString)
	tools.WriteLogToDB(loggerToDBObj, r)
	fmt.Fprintf(w, string(jsonResp))
	//log.Println(string(logString))
	//log.Println(string(logString))
	/*rows, err := tools.DB.Table("users").Rows()
	if err != nil {
		w.WriteHeader(http.StatusServiceUnavailable)
		loggerToDBObj.Status=strconv.Itoa(http.StatusServiceUnavailable)
		checkError.ErrorCode=3
		checkError.ErrorMessage=err.Error()
		jsonResp, _ := json.Marshal(checkError)
		logObject.Resp.RespBodyString = string(jsonResp)
		logString, _ := json.Marshal(logObject)
		loggerToDBObj.Data=string(logString)
		tools.WriteLogToDB(loggerToDBObj, r)
		//log.Println(string(logString))
		fmt.Fprintf(w, string(jsonResp))
		log.Fatal(err)
		return
	}
	defer rows.Close()
	for rows.Next() {
		if err := rows.Scan(&userid, &userName, &passWord); err != nil {
			w.WriteHeader(http.StatusServiceUnavailable)
			loggerToDBObj.Status=strconv.Itoa(http.StatusServiceUnavailable)
			checkError.ErrorCode=3
			checkError.ErrorMessage=err.Error()
			jsonResp, _ := json.Marshal(checkError)
			logObject.Resp.RespBodyString = string(jsonResp)
			logString, _ := json.Marshal(logObject)
			loggerToDBObj.Data=string(logString)
			tools.WriteLogToDB(loggerToDBObj, r)
			//log.Println(string(logString))
			fmt.Fprintf(w, string(jsonResp))
			log.Fatal(err)
		}
		if userInput.Username == userName && userInput.Password == passWord{
			var checkLoginError LoginErrorHandler
			userToken = userid
			token := jwt.New(jwt.GetSigningMethod("RS256"))
			claims := token.Claims.(jwt.MapClaims)
			claims["userID"] = userToken
			claims["exp"] = time.Now().Add(time.Hour * 24).Unix()
			tokenString, _ := token.SignedString(privateKey)
			checkLoginError.ErrorCode=0
			checkLoginError.ErrorMessage="success"
			checkLoginError.Context.JwtToken=tokenString
			jsonResp,_ := json.Marshal(checkLoginError)
			logObject.Resp.RespBodyString = string(jsonResp)
			logString, _ := json.Marshal(logObject)
			loggerToDBObj.Data=string(logString)
			tools.WriteLogToDB(loggerToDBObj, r)
			//log.Println(string(logString))
			fmt.Fprintf(w, string(jsonResp))
			return
		}
	}
	checkError.ErrorMessage = "Username or Password does not match"
	checkError.ErrorCode = 3
	w.WriteHeader(http.StatusBadRequest)
	loggerToDBObj.Status=strconv.Itoa(http.StatusBadGateway)
	jsonResp, _ := json.Marshal(checkError)
	logObject.Resp.RespBodyString = string(jsonResp)
	logString, _ := json.Marshal(logObject)
	loggerToDBObj.Data=string(logString)
	tools.WriteLogToDB(loggerToDBObj, r)
	fmt.Fprintf(w, string(jsonResp))
	*/
}