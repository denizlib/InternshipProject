package main

import (
	"fmt"
	"net/http"
	"log"
	"orangenote/notes"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
	"os"

)

func main() {
	router := mux.NewRouter()
	n := negroni.Classic()

	router.HandleFunc("/note/login", notes.LoginFunc)
	router.HandleFunc("/note/register", notes.RegisterFunc)
	router.Handle("/note/add", negroni.New(negroni.HandlerFunc(notes.AuthMiddleware), negroni.Wrap(http.HandlerFunc(notes.AddNote))))
	router.Handle("/note/show/all", negroni.New(negroni.HandlerFunc(notes.AuthMiddlewareForGetNotes), negroni.Wrap(http.HandlerFunc(notes.GetNotes))))
	router.Handle("/note/show/{id}", negroni.New(negroni.HandlerFunc(notes.AuthMiddleware), negroni.Wrap(http.HandlerFunc(notes.GetSingleNote))))
	router.Handle("/note/update/{id}", negroni.New(negroni.HandlerFunc(notes.AuthMiddleware), negroni.Wrap(http.HandlerFunc(notes.UpdateNote))))
	router.Handle("/note/delete/{id}", negroni.New(negroni.HandlerFunc(notes.AuthMiddleware), negroni.Wrap(http.HandlerFunc(notes.DeleteNote))))
	router.HandleFunc("/note/changepw/request", notes.ChangePasswordRequest)
	router.Handle("/note/changepw/confirm", negroni.New(negroni.HandlerFunc(notes.AuthMiddleware), negroni.Wrap(http.HandlerFunc(notes.ChangePasswordConfirmation))))
	router.Handle("/note/options/{id}", negroni.New(negroni.HandlerFunc(notes.AuthMiddleware), negroni.Wrap(http.HandlerFunc(notes.SetOptions))))

	n.UseHandler(router)

	addr, err := determineListenAddress()
	if err != nil {
		log.Fatal(err)
	}
	if err := http.ListenAndServe(addr, n); err != nil {
		panic(err)
	}
}

func determineListenAddress() (string, error) {
	port := os.Getenv("PORT")
	if port == "" {
		return "", fmt.Errorf("$PORT not set")
	}
	return ":" + port, nil
}
