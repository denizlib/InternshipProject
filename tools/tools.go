package tools

import (
	"regexp"
	"net/http"
	"fmt"
	"strings"
	"io/ioutil"
	"bytes"
	"sync"
	"log"
	"time"
	"github.com/jinzhu/gorm"
)
type LogDataStruct struct{
	Req		ReqStruct				`json:"request"`
	Resp	RespStruct				`json:"response"`
}

type ReqStruct struct{
	Header []string					`json:"header"`
	Body   string	 				`json:"body"`
}


type RespStruct struct{
	RespBodyString string 			`json:"body"`
}

type LoggerToDB struct{
	gorm.Model
	IP				string
	Status 			string
	Method			string
	Path			string
	Data			string
}

func ValidateEmail(email string) bool {
	regExp := regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
	return regExp.MatchString(email)
}

func FormatRequest(r *http.Request) LogDataStruct {
	var logger LogDataStruct
	var requestArray []string
	url := fmt.Sprintf("%v %v %v", r.Method, r.URL, r.Proto)
	requestArray = append(requestArray, url)
	requestArray = append(requestArray, fmt.Sprintf("Host: %v", r.Host))
	for name, headers := range r.Header {
		name = strings.ToLower(name)
		for _, h := range headers {
			requestArray = append(requestArray, fmt.Sprintf("%v: %v", name, h))
		}
	}
	// If this is a POST, add post data
	//if r.Method == "POST" {
	//r.ParseForm()
	//requestArray = append(requestArray, "\n")
	//requestArray = append(requestArray, r.Form.Encode())
	//}
	// Return the requestArray as a string
	//jsonString,_ := json.Marshal(requestArray)
	//return string(jsonString)
	//return strings.Join(requestArray, "\n")
	logger.Req.Header= requestArray
	//defer r.Body.Close()
	body,_ := ioutil.ReadAll(r.Body)
	logger.Req.Body=string(body)
	r.Body = ioutil.NopCloser(bytes.NewBuffer(body))
	return logger
}

func WriteLogToDB(loggerToDBObj LoggerToDB, r *http.Request){
	tx := DB.Begin().Table("logs")
	if err := tx.Create(&loggerToDBObj).Error; err != nil{
		tx.Rollback()
		var mutex = &sync.Mutex{}
		mutex.Lock()
		errBuf, err := ioutil.ReadFile("specialerrorlog.txt")
		if err != nil {
			log.Println("DOOOOOOOOOOOOOOOOOOOOM!!")
			panic(err)
		}
		s := string(errBuf[:])
		url := fmt.Sprintf("%v | %v | %v", r.Method, r.URL, r.Proto)
		s = s + "\n" + time.Now().String() + " | " + r.RemoteAddr + " | " + url
		err = ioutil.WriteFile("specialerrorlog.txt", []byte(s), 0644)
		if err != nil {
			log.Println("Doom Upon Doom!!")
			panic(err)
		}
		mutex.Unlock()
		log.Println(err.Error())
		return
	}
	tx.Commit()
}

func (LoggerToDB) TableName() string{
	return "logs"
}